﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo1.Areas.Admin.Controllers
{
    public class LichThiDauController : Controller
    {
        // GET: Admin/LichThiDau
        public ActionResult LichBongDa()
        {
            return View();
        }
        public ActionResult LichBongChuyen()
        {
            return View();
        }
        public ActionResult LichCauLong()
        {
            return View();
        }
        public ActionResult LichKeoCo()
        {
            return View();
        }
        public ActionResult LichCoVua_CoTuong()
        {
            return View();
        }
        public ActionResult LichDienKinh()
        {
            return View();
        }
    }
}